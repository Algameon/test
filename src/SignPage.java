import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Servlet implementation class SignPage.
 *
 * @author Martin Host
 * @version 1.0
 */
@WebServlet("/SignPage")
public class SignPage extends servletBase {

    /**
     * @see servletBase#servletBase()
     */
    public SignPage() {

        super();
    }


    /**
     * Accepts a weekreport in the database.
     * If the user does not exist in the database nothing happens.
     */

    private String printProjectReports(String project) {
        String html = "<table border=\" + formElement(\"1\") + \">\n" +
                "      <tr>\n" +
                "      <th>Namn</th>\n" +
                "      <th>Vecka</th>\n" +
                "      <th>Status</th> \n" +
                "      <th>Visa</th> \n" +
                "      <th>Acceptera</th> \n" +
                "      <th>Neka</th>\n" +
                "      <th>Skicka</th>\n" +
                "      <th>Kommentar</th>\n" +
                "      </tr>";

        html += Database.getAllProjectReports(project, conn);
        html += "</table>";
        return html;
    }

    /**
     * Prints out user report
     */
    private String printUserReports(String name) {
        String html = "<table border=\" + formElement(\"1\") + \">\n" +
                "      <tr>\n" +
                "      <th>Namn</th>\n" +
                "      <th>Vecka</th>\n" +
                "      <th>Status</th> \n" +
                "      <th>Visa</th> \n" +
                "      <th>Acceptera</th> \n" +
                "      <th>Neka</th>\n" +
                "      <th>Skicka</th>\n" +
                "      <th>Kommentar</th>\n" +
                "      </tr>";

        html += Database.getAllUserReportsComplete(name, conn);
        html += "</table>";
        return html;
    }


    /**
     * Prints out selected users report
     */

    protected String showSelectedUserReport(String project) {
        String html = "<p>Välj vilken användares tidrapporter du vill se: </p>";
        html += "<p><form>";
        html += "<select name=\"user\">";

        for (int i = 0; i < Database.getUsers(project, conn).size(); i++) {
            html += "<td><option value=" + Database.getUsers(project, conn).get(i) + ">" + Database.getUsers(project, conn).get(i) + "</td></tr></br>";
        }
        html += "<input type=" + formElement("submit") + "value=\"Välj\">";
        html += "</select></form>";

        return html;
    }

    /**
     * Handles input from the user and displays information.
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        PrintWriter out = response.getWriter();
        if(loggedIn(request)) {
            out.println(getPageIntro());
            out.println(getFunctionMenu());
            out.println("<h1>Signera " + "</h1>");


            String project;
            HttpSession session = request.getSession(true);
            project = session.getAttribute("project").toString();

            boolean choose = false;
            String userReports = "";
            String username = (String) session.getAttribute("name");
            int userRole = Database.getRole(username, conn);

            if (userRole == 0 || userRole == 100) {
                if (request.getParameter("function") != null && request.getParameter("week") != null && request.getParameter("user") != null) {
                    if (request.getParameter("function").equals("Accept")) {
                        String comm = request.getParameter("comment");
                        int week = Integer.parseInt(request.getParameter("week"));
                        String user = request.getParameter("user");
                        Database.acceptReport(user, week, conn);
                        Database.setComment(comm, user, week, conn);
                    } else if (request.getParameter("function").equals("Deny")) {
                        String com = request.getParameter("comment");
                        int week = Integer.parseInt(request.getParameter("week"));
                        String user = request.getParameter("user");
                        Database.denyReport(user, week, conn);
                        Database.setComment(com, user, week, conn);
                    }
                } else if (request.getParameter("comment") != null && request.getParameter("week") != null && request.getParameter("user") != null) {
                    String user = request.getParameter("user");
                    int week = Integer.parseInt(request.getParameter("week"));
                    String comment = request.getParameter("comment");
                    Database.setComment(comment, user, week, conn);
                } else if (request.getParameter("user") != null) {
                    choose = true;
                }

                if (project != null) {
                    if (choose) {
                        out.println(printUserReports(request.getParameter("user")));
                    } else {
                        out.println(printProjectReports(project));
                    }
                    out.println(showSelectedUserReport(project));


                    out.println("</body></html>");
                }
            } else {
                response.sendRedirect("MainPage");
            }
        }else{
            response.sendRedirect("");
        }
    }

    /**
     *
     */

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}