import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * Servlet implementation class ProjectPage.
 *  @author Martin Host
 *  @version 1.0
 */
@WebServlet("/ProjectPage")
public class ProjectPage extends servletBase {

    /**
     * @see servletBase#servletBase()
     */
    public ProjectPage() {
        super();
    }


    /**
     * Genereates html-code to create the roleChange form.
     * @return String that contains the html-code.
     */
    protected String roleChangeForm(){
        String html = "<p>Skriv in användarnamn och roll för att ändra roll:</p>";
        html += "<p> <form name=" + formElement("input");
        html += " method=" + formElement("post");
        html += "<p> Namn: <input type=" + formElement("text") + " name=" + formElement("name") + '>';
        html += "<select name=\"role\"> " +
                "   <option value=\"0\">Projektledare</option>" +
                "   <option value=\"1\">Systemvetare</option>" +
                "   <option value=\"2\">Utvecklare</option>" +
                "   <option value=\"3\">Testare</option>" +
                "</select>";
        html += "<p> <input type=" + formElement("submit") + "value=" + formElement("Ändra") + '>';
        return html;
    }

    /**
     * Handles input from the user and displays information.
     *
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        PrintWriter out = response.getWriter();
        if(loggedIn(request)) {
            out.println(getPageIntro());
            out.println(getFunctionMenu());

            out.println("<h1>Projektsida " + "</h1>");
            out.println("</body></html>");

            String name;
            String role;

            HttpSession session = request.getSession(true);
            String username = (String) session.getAttribute("name");
            int userRole = Database.getRole(username, conn);

            if (userRole == 0 || userRole == 100) {
                name = request.getParameter("name"); // get the string that the user entered in the form
                role = request.getParameter("role"); // get the entered role
                System.out.println(role);

                if (name != null) {
                    Database.changeRole(name, Integer.parseInt(role), conn);
                    out.println(getPageIntro());
                    out.println("Role has been updated");

                } else { // name was null, probably because no form has been filled out yet. Display form.
                    out.println(roleChangeForm());
                }
            } else {
                response.sendRedirect("MainPage");
            }
        }else {
            response.sendRedirect("");
        }
    }

    /**
     *
     */


    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request, response);
    }
}