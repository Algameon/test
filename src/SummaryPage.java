import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Servlet implementation class SummaryPage.
 * @author Martin Host
 * @version 1.0
 */
@WebServlet("/SummaryPage")
public class SummaryPage extends servletBase {

    /**
     * @see servletBase#servletBase()
     */
    public SummaryPage() {
        super();
    }

    /**
     * Handles table outprint and buttonfunction
     */
    private String printWeekSummary(String project, int sum) {
        String html = "<h1>Summering " + "</h1>"+ "<form action=\"SummaryPage\"> ";
         html +=        "<table border=\" + formElement(\"1\")>" +
                "      <tr>" +
                "      <th>Vecka</th>" +
                "     <th>Visa</th></tr>";
        for (int i = 0; i < Database.getWeeksOfProject(project, conn).size(); i++) {
            html += "<td>" + Database.getWeeksOfProject(project, conn).get(i) + " </td>" + "<td><input type=\"checkbox\" name=\"week\" value=" +
                    Database.getWeeksOfProject(project, conn).get(i) + "></td></tr>";
        }
        html += "<th>SUM: </th><th>" + sum + " </th></table>";
        html += "<button onclick=\"mySummaryFunction()\">Visa summering</button>";
        html += "</form>";
        html += "<form>";
        html += "<input type=" + formElement("submit") + "value=\"Visa samtliga veckosummeringar\">" +
                "<input type=\"hidden\" name=\"all\" value=\"all\">";
        html += "</table></form>";

        return html;
    }

    /**
     * Handles input from the user and displays information.
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        PrintWriter out = response.getWriter();
        if(loggedIn(request)) {
            out.println(getPageIntro());
            out.println(getFunctionMenu());
            HttpSession session = request.getSession(true);
            String project = session.getAttribute("project").toString();
            int result = 0;

            String username = (String) session.getAttribute("name");
            int role = Database.getRole(username, conn);

            if (role == 0) {
                if (request.getParameterValues("week") != null) {
                    String[] weeks = request.getParameterValues("week");
                    for (String w : weeks) {
                        result += Database.getSpecificWeekReports(project, w, conn);
                    }
                }
                if (request.getParameter("all") != null) {
                    result = Database.getAllWeekReports(project, conn);
                }
                out.println(printWeekSummary(project, result));
                out.println("</body></html>");

            } else {
                out.println("<script>alert(\"" + "Ej korrekt behörighet\")</script>");
            }
        }else{
            response.sendRedirect("");
        }
    }


    /**
     * All requests are forwarded to the doGet method.
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request, response);
    }
}