import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * Servlet implementation class ReportHistoryPage.
 *  @author Martin Host
 *  @version 1.0
 */
@WebServlet("/ReportHistoryPage")
public class ReportHistoryPage extends servletBase {

    /**
     * @see servletBase#servletBase()
     */
    public ReportHistoryPage() {
        super();
    }


    /**
     * Deletes a weekreport by the specified user on the specified week.
     * If the user does not exist in the database nothing happens.
     * @param name name of user.
     * @param week week that is to be removed.
     */
    private void deleteReport(String name, int week) {
        try{
            Statement stmt = conn.createStatement();
            String statement = "delete from weeklyReports where user = '" + name + "' and week = '" + week + "'";
            System.out.println(statement);
            stmt.executeUpdate(statement);
            stmt.close();

        } catch (SQLException ex) {
            System.out.println("SQLException: " + ex.getMessage());
            System.out.println("SQLState: " + ex.getSQLState());
            System.out.println("VendorError:  " + ex.getErrorCode());
        }
    }

    private String printReports(String name) {
        String html = "<table border=\" + formElement(\"1\") + \">\n" +
                "      <tr>\n" +
                "      <th>Vecka</th>\n" +
                "      <th>Status</th> \n" +
                "      <th>Visa</th> \n" +
                "      <th>Uppdatera</th> \n" +
                "      <th>Kommentar</th> \n" +

                "      </tr>";
        html += Database.getAllUserReports(name, conn);
        html += "</table>";
        return html;
    }

    /**
     * Handles input from the user and displays information.
     *
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession(true);
        PrintWriter out = response.getWriter();
        if (loggedIn(request)) {

            out.println(getPageIntro());
            out.println(getFunctionMenu());
            out.println("<h1>Historik " + "</h1>");

            String name;

            name = session.getAttribute("name").toString(); // get the string that the user entered in the form
            out.println(printReports(name));
            out.println("</body></html>");
        } else {
            response.sendRedirect("");
        }
    }
    /**
     *
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}