import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.crypto.Data;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * Servlet implementation class Administration.
 * Constructs a page for administration purpose.
 * Checks first if the user is logged in and then if it is the administrator.
 * If that is OK it displays all users and a form for adding new users.
 *
 * @author Martin Host
 * @version 1.0
 */
@WebServlet("/Administration")
public class Administration extends servletBase {
    private static final long serialVersionUID = 1L;


    /**
     * @see servletBase#servletBase()
     */
    public Administration() {
        super();
    }

    /**
     * generates a form for adding new users
     *
     * @return HTML code for the form
     */
    private String addUserForm() {
        String html = "<meta charset=\"utf-8\">";
        html += "<p> <form name=" + formElement("input");
        html += " method=" + formElement("get");
        html += "<p>Lägg till användare: <input type=" + formElement("text") + " name=" + formElement("addname") + '>';
        html += "<input type=" + formElement("submit") + "value=" + formElement("Lägg till användare") + '>';
        html += "</form>";
        return html;
    }

    /**
     * Checks if a username corresponds to the requirements for user names.
     *
     * @param name The investigated username
     * @return True if the username corresponds to the requirements
     */
    private boolean checkNewName(String name) {
        int length = name.length();
        boolean ok = (length >= 5 && length <= 10);
        if (ok)
            for (int i = 0; i < length; i++) {
                int ci = (int) name.charAt(i);
                boolean thisOk = ((ci >= 48 && ci <= 57) ||
                        (ci >= 65 && ci <= 90) ||
                        (ci >= 97 && ci <= 122));
                ok = ok && thisOk;
            }
        return ok;
    }

    /**
     * Add a user to a project.
     *
     * @return a HTML code for the form.
     */

    private String addAssignProjectForm() {
        String html;
        html = "<p> <form name=" + formElement("input");
        html += " method=" + formElement("get");
        html += "<p> Användare: <input type=" + formElement("text") + " name=" + formElement("assignUser") + '>';
        html += " Projekt: <input type=" + formElement("text") + " name=" + formElement("assignProject") + '>';
        html += "<input type=" + formElement("submit") + "value=" + formElement("Tilldela") + '>';
        html += "</form>";
        return html;
    }

    /**
     * Handles input from the user and displays information for administration.
     * First it is checked if the user is logged in and that it is the administrator.
     * If that is the case all users are listed in a table and then a form for adding new users is shown.
     * Inputs are given with two HTTP input types:
     * addname: name to be added to the database (provided by the form)
     * deactivename: name to be deactived from the database (provided by the URLs in the table)
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        PrintWriter out = response.getWriter();
        if (loggedIn(request)) {
            out.println(getPageIntro());
            out.println(getFunctionMenu());

            String myName = "";
            HttpSession session = request.getSession(true);
            Object nameObj = session.getAttribute("name");
            if (nameObj != null)
                myName = (String) nameObj;  // if the name exists typecast the name to a string

            // check that the user is logged in
            //if (!loggedIn(request))
            //   response.sendRedirect("LogIn");
            if (myName.equals("admin")) {
                out.println("<h1>Administrationssida" + "</h1>");

                // check if the administrator wants to add a new user in the form

                // check if the administrator wants to add a new user in the form
                String newName = request.getParameter("addname");
                if (newName != null) {
                    if (checkNewName(newName)) {
                        boolean addPossible = Database.addUser(newName, conn);
                        if (!addPossible) {
                            out.println("<p>Error: Valda användarnamnet går ej att lägga till</p>");
                            out.println("<script>alert(\"" + "Användaren finns redan\")</script>");
                        } else {
                            out.println("<p>Användaren " + newName + " lades till</p>");
                        }
                    } else if (newName.length() < 5) {
                        out.println("<p>Error: För kort användarnamn</p>");
                        out.println("<script>alert(\"" + "För kort användarnamn\")</script>");
                    } else if (newName.length() > 11) {
                        out.println("<p>Error: För långt användarnamn</p>");
                        out.println("<script>alert(\"" + "För långt användarnamn\")</script>");
                    } else {
                        out.println("<p>Error: Otillåtna tecken använda</p>");
                        out.println("<script>alert(\"" + "Otillåtna tecken använda\")</script>");
                    }
                }


                String projectName = request.getParameter("addProject");
                if (projectName != null) {
                    if (checkNewName(projectName)) {
                        if (Database.createProject(projectName, conn)) {
                            out.println("<p>Projektnamn " + projectName + " lades till</p>");
                        } else {
                            out.println("<script>alert(\"" + projectName + "Gick inte att lägga till\")</script>");
                        }
                    } else {
                        out.println("<script>alert(\"" + projectName + " är inte ett tillåtet namn på projekt\")</script>");
                    }
                }


                String assignUser = request.getParameter("assignUser");
                String assignProject = request.getParameter("assignProject");

                if (assignUser != null && assignProject != null) {
                    if (Database.assignUserToProject(assignUser, assignProject, conn)) {
                        out.println("<p>Användaren " + assignUser + " lades till i projektet " + assignProject + "</p>");
                    } else {
                        out.println("<script>alert(\"Gick inte att lägga till " + assignUser + " i " + assignProject + "\")</script>");
                    }
                }

                String deactivateName = request.getParameter("deactivename");
                if (deactivateName != null){
                    Database.deactivateUser(deactivateName, conn);
                }


                try {
                    Statement stmt = conn.createStatement();
                    ResultSet rs = stmt.executeQuery("select * from Users order by username asc");
                    out.println("<p>Registrerade användare:</p>");
                    out.println("<table border=" + formElement("1") + ">");
                    out.println("<tr><th>Namn</td><th>Lösenord</td><th>Projekt</th><th></th><th>Användarroll</td></tr>");
                    while (rs.next()) {
                        String name = rs.getString("username");
                        int role = rs.getInt("role");
                        int activated = rs.getInt("activated");
                        String userRole = roleToString(role);

                        if (!name.equals("admin") && activated != 0) {

                            String pw = rs.getString("password");
                            String project = rs.getString("project");
                            String deactiveURL = "Administration?deactivename=" + name;
                            String deactiveCode = "<a href=" + formElement(deactiveURL) +
                                    " onclick=" + formElement("return confirm('Är du säker på att du vill inaktivera användaren: " + name + "?')") +
                                    "> inaktivera </a>";

                            out.println("<tr>");
                            out.println("<td>" + name + "</td>");
                            out.println("<td>" + pw + "</td>");
                            out.println("<td>" + project + "</td>");
                            out.println("<td>" + deactiveCode + "</td>");
                            out.println("<td>" + userRole + "</td>");
                            out.println("</tr>");
                        }
                    }
                    out.println("</table>");
                    stmt.close();
                } catch (SQLException ex) {
                    System.out.println("SQLException: " + ex.getMessage());
                    System.out.println("SQLState: " + ex.getSQLState());
                    System.out.println("VendorError: " + ex.getErrorCode());
                }

                out.println(addUserForm());
                out.println(Database.projectForm(conn));
                out.println(addProjectForm());
                out.println(addAssignProjectForm());
                out.println("</body></html>");
            } else {
                out.println("<script>alert(\"" + "Ej korrekt behörighet\")</script>");
                response.sendRedirect("MainPage");
            }
        }
    }


    /**
     * Adds a new project
     *
     * @return HTML code for the form
     */
    private String addProjectForm() {
        String html;
        html = "<p> <form name=" + formElement("input");
        html += " method=" + formElement("get");
        html += "<p> Projektnamn: <input type=" + formElement("text") + " name=" + formElement("addProject") + '>';
        html += "<input type=" + formElement("submit") + "value=" + formElement("Lägg till projekt") + '>';
        html += "</form>";
        return html;
    }

    private String roleToString(int i) {
        String s = "";

        if (i == 0) {
            s = "Projektledare";
        } else if (i == 1) {
            s = "Systemvetare";
        } else if (i == 2) {
            s = "Utvecklare";
        } else if (i == 3) {
            s = "Testare";
        } else if (i == 100) {
            s = "Admin";
        } else {
            s = "Användare";
        }

        return s;
    }
}

