import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Servlet implementation class LogIn
 * <p>
 * A log-in page.
 * <p>
 * The first thing that happens is that the user is logged out if he/she is logged in.
 * Then the user is asked for name and password.
 * If the user is logged in he/she is directed to the functionality page.
 *
 * @author Martin Host
 * @version 1.0
 */
@WebServlet("/")
public class LogIn extends servletBase {
    private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public LogIn() {
        super();
    }

    /**
     * Generates a form for login.
     *
     * @return HTML code for the form
     */
    protected String loginRequestForm() {
        String html ="<body BGCOLOR=\"#add8e6\">\n" +
                "<!-- menu row --><TR VALIGN=top>\n" +
                "    <TD NOWRAP>\n" + "<p>Skriv in ditt användarnamn och lösenord:</p>";
        html += "<p> <form name=" + formElement("input");
        html += " method=" + formElement("post");
        html += "<p> Namn: <input type=" + formElement("text") + " name=" + formElement("user") + '>';
        html += "<p> Lösenord: <input type=" + formElement("password") + " name=" + formElement("password") + '>';
        html += "<p> <input type=" + formElement("submit") + "value=" + formElement("Logga in") + '>';
        return html;
    }

    /**
     * Implementation of all input to the servlet. All post-messages are forwarded to this method.
     * <p>
     * First logout the user, then check if he/she has provided a username and a password.
     * If he/she has, it is checked with the database and if it matches then the session state is
     * changed to login, the username that is saved in the session is updated, and the user is
     * relocated to the functionality page.
     *
     * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        // Get the session

        HttpSession session = request.getSession(true);
        //session.setAttribute("state", 0);

        PrintWriter out = response.getWriter();
        out.println(getPageIntro());


        String name;
        String password;

        name = request.getParameter("user"); // get the string that the user entered in the form
        password = request.getParameter("password"); // get the entered password
        if (name != null && password != null) {
            if (Database.checkUser(name, password, conn)) {
                session.setAttribute("state", LOGIN_TRUE);  // save the state in the session
                session.setAttribute("name", name);  // save the name in the session
                session.setAttribute("role", Database.getRole(name, conn));
                session.setAttribute("project", Database.getProject(name,conn));
                response.sendRedirect("MainPage");
            } else {

                out.println("<p style=\"color:red\";>Felaktigt användarnamn eller lösenord. </p>");
                out.println("<script>alert(\"Du har angivit fel användarnamn eller lösenord\")</script>");
                out.println(loginRequestForm());
            }
        } else { // name was null, probably because no form has been filled out yet. Display form.
            out.println(loginRequestForm());
        }

        out.println("</body></html>");

    }
    /**
     *
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request, response);
    }

}
