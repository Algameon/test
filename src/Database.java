import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Random;


import javax.servlet.ServletException;
import java.util.*;
import java.sql.*;

public class Database {
    private static final int PASSWORD_LENGTH = 6;


    /**
     * Adds a user and a randomly generated password to the database.
     *
     * @param name Name to be added
     * @return true if it was possible to add the name. False if it was not, e.g.
     * because the name already exist in the database.
     */
    static boolean addUser(String name, Connection conn) {
        boolean resultOk = true;
        PreparedStatement ps = null;

        try {

            String sql = "INSERT INTO Users (username, password, project, role) VALUES (?, '" + createPassword() + "', 'Default', '-1')";
            ps = conn.prepareStatement(sql);
            ps.setString(1, name);
            ps.executeUpdate();
            ps.close();

        } catch (SQLException ex) {
            resultOk = false;
            printException(ex);
        }
        return resultOk;
    }

    /**
     * Assign user to a project
     *
     * @param name    Name to be assigned.
     * @param project Name of the project that user will be assigned to.
     * @return true if it was possible to assign user to the project. False if it was not.
     */

    static boolean assignUserToProject(String name, String project, Connection conn) {
        boolean resultOk = true;
        PreparedStatement ps = null;

        try {
            String sql = "UPDATE Users SET project = ? WHERE username = ?";

            ps = conn.prepareStatement(sql);
            ps.setString(1, project);
            ps.setString(2, name);
            ps.executeUpdate();
            ps.close();

        } catch (SQLException ex) {

            resultOk = false;
            printException(ex);
        }
        return resultOk;
    }


    /**
     * Create a project with a name
     *
     * @param pName Name of the project that will be created.
     * @return true if it was possible to create the project. False if it was not, e.g,
     * A project with that name already exists.
     */


    static boolean createProject(String pName, Connection conn) {
        PreparedStatement ps = null;
        boolean resultOK = true;

        try {
            String sql = "INSERT INTO Projects(projectName) VALUES(?)";
            ps = conn.prepareStatement(sql);
            ps.setString(1, pName);
            ps.executeUpdate();
            ps.close();
        } catch (SQLException ex) {
            resultOK = false;
            printException(ex);
        }
        return resultOK;
    }


    /**
     * Creates a random password.
     *
     * @return a randomly chosen password
     */
    private static String createPassword() {
        StringBuilder sb = new StringBuilder();
        Random r = new Random();
        for (int i = 0; i < PASSWORD_LENGTH; i++) {
            sb.append((char) (r.nextInt(26) + 97)); // 122-97+1=26
        }
        return sb.toString();

    }


    /**
     * Deactivate a user.
     *
     * @param name Name of the user that will be deactivated.
     */

    static void deactivateUser(String name, Connection conn) {
        PreparedStatement ps = null;
        try {
            String sql = "UPDATE Users SET activated = 0 WHERE username= ?";
            ps = conn.prepareStatement(sql);
            ps.setString(1, name);
            ps.executeUpdate();
            ps.close();
//            System.out.println(sql);


        } catch (SQLException ex) {
            printException(ex);
        }
    }

    /**
     * Leave a comment on the week report.
     *
     * @param comment The comment to the week report.
     * @param user    Name of the owner to the week report.
     * @param week    The week for the week report.
     */

    public static void setComment(String comment, String user, int week, Connection conn) {
        PreparedStatement ps = null;
        String com = comment;
        try {
            String sql = "UPDATE weeklyReports SET comments = ? WHERE username = ? AND week = ?";
            ps = conn.prepareStatement(sql);
            ps.setString(1, comment);
            ps.setString(2, user);
            ps.setInt(3, week);
            ps.executeUpdate();
            ps.close();

        } catch (SQLException ex) {
            System.out.println("SQLException: " + ex.getMessage());
            System.out.println("SQLState: " + ex.getSQLState());
//            System.out.println("VendorError: " + ex.getErrorCode());
        }
    }


    /**
     * Checks with the database if the user should be accepted
     *
     * @param name     The name of the user
     * @param password The password of the user
     * @return true if the user should be accepted
     */

    static boolean checkUser(String name, String password, Connection conn) {

        boolean userOk = false;
        PreparedStatement ps = null;

        try {
            String sql = "SELECT username, password FROM Users WHERE username = ? AND activated = '1'";
            ps = conn.prepareStatement(sql);
            ps.setString(1, name);

            ResultSet rs = ps.executeQuery();

            while (rs.next()) {
                if (rs.getString("password").equals(password)) {
                    userOk = true;
                }
            }
            ps.close();
        } catch (SQLException ex) {
            printException(ex);
        }
        return userOk;
    }

    /**
     * Change role of a user.
     *
     * @param name Name of the user that will change role.
     * @param role Which role the user will get.
     * @return true if it was possible to change a users role. False if it was not.
     */

    /**
     * Changes the role of a user in the database.
     * If the user does not exist in the database nothing happens.
     *
     * @param name name of user.
     * @param role to be assigned to the user.
     */
    static boolean changeRole(String name, int role, Connection conn) {
        PreparedStatement ps = null;
        boolean done = false;
        try {
            String sql = "UPDATE Users SET role = ? WHERE username= ?";
            ps = conn.prepareStatement(sql);
            ps.setInt(1, role);
            ps.setString(2, name);
            ps.executeUpdate();
            ps.close();
            done = true;

        } catch (SQLException ex) {
            printException(ex);
            done = false;
        }
        return done;
    }

    /**
     * Create a new report.
     *
     * @param week             Week of the report.
     * @param user             Name of the user.
     * @param SDP              Time of working with SDP.
     * @param SRS              Time of working with SRS.
     * @param SVVS             Time of working with SVVS.
     * @param STLDD            Time of working with STLDD.
     * @param SVVI             Time of working with SVVI.
     * @param SDDD             Time of working with SDDD.
     * @param SVVR             Time of working with SVVR.
     * @param SSD              Time of working with SSD.
     * @param PFR              Time of working with PFR.
     * @param functionalTest   Time of working with functionalTest.
     * @param systemTest       Time of working with systemTest.
     * @param regressionTest   Time of working with refressionTest.
     * @param meeting          Time of working with meeting.
     * @param lecture          Time of working with lecture.
     * @param exercise         Time of working with exercise.
     * @param computerExercise Time of working with computerExericse.
     * @param homeReading      Time of working with homeReading.
     * @param other            Time of working with other.
     * @return true if it was possible to create a report. False if it was not.
     */

    static boolean createNewReport(int week, String user, int SDP, int SRS, int SVVS, int STLDD, int SVVI
            , int SDDD, int SVVR, int SSD, int PFR, int functionalTest, int systemTest, int regressionTest, int meeting
            , int lecture, int exercise, int computerExercise, int homeReading, int other, Connection conn) {
        PreparedStatement ps = null;
        boolean resultOk = true;

        try {
            String sql = "INSERT INTO weeklyReports VALUES(?, '0', NULL, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?," +
                    " ?, ?, ?, ?, ?)";
            ps = conn.prepareStatement(sql);
            ps.setInt(1, week);
            ps.setString(2, user);
            ps.setInt(3, SDP);
            ps.setInt(4, SRS);
            ps.setInt(5, SVVS);
            ps.setInt(6, STLDD);
            ps.setInt(7, SVVI);
            ps.setInt(8, SDDD);
            ps.setInt(9, SVVR);
            ps.setInt(10, SSD);
            ps.setInt(11, PFR);
            ps.setInt(12, functionalTest);
            ps.setInt(13, systemTest);
            ps.setInt(14, regressionTest);
            ps.setInt(15, meeting);
            ps.setInt(16, lecture);
            ps.setInt(17, exercise);
            ps.setInt(18, computerExercise);
            ps.setInt(19, homeReading);
            ps.setInt(20, other);

            ps.executeUpdate();
            ps.close();
        } catch (SQLException ex) {
            printException(ex);
            resultOk = false;
        }
        return resultOk;
    }

    /**
     * Update a existing report.
     *
     * @param week             Week of the report.
     * @param user             Name of the user.
     * @param SDP              Time of working with SDP.
     * @param SRS              Time of working with SRS.
     * @param SVVS             Time of working with SVVS.
     * @param STLDD            Time of working with STLDD.
     * @param SVVI             Time of working with SVVI.
     * @param SDDD             Time of working with SDDD.
     * @param SVVR             Time of working with SVVR.
     * @param SSD              Time of working with SSD.
     * @param PFR              Time of working with PFR.
     * @param functionalTest   Time of working with functionalTest.
     * @param systemTest       Time of working with systemTest.
     * @param regressionTest   Time of working with refressionTest.
     * @param meeting          Time of working with meeting.
     * @param lecture          Time of working with lecture.
     * @param exercise         Time of working with exercise.
     * @param computerExercise Time of working with computerExericse.
     * @param homeReading      Time of working with homeReading.
     * @param other            Time of working with other.
     * @return true if it was possible to create a report. False if it was not.
     */

    static boolean updateReport(int week, String user, int SDP, int SRS, int SVVS, int STLDD, int SVVI
            , int SDDD, int SVVR, int SSD, int PFR, int functionalTest, int systemTest, int regressionTest, int meeting
            , int lecture, int exercise, int computerExercise, int homeReading, int other, Connection conn) {
        PreparedStatement ps = null;
        boolean resultOk = true;

        try {
            String sql = "UPDATE weeklyReports SET SDP = ? , SRS = ? , SVVS = ? , STLDD = ? , SVVI = ? , " +
                    "SDDD = ? , SVVR = ? , SSD= ? , PFR= ? ,functionalTest = ? , systemTest = ? ,regressionTest = ? , meeting = ? , lecture = ? ," +
                    " exercise = ? , computerExercise = ? , homeReading = ? , other = ? " +
                    "WHERE username = ? AND week = ?";
            ps = conn.prepareStatement(sql);
            ps.setInt(1, SDP);
            ps.setInt(2, SRS);
            ps.setInt(3, SVVS);
            ps.setInt(4, STLDD);
            ps.setInt(5, SVVI);
            ps.setInt(6, SDDD);
            ps.setInt(7, SVVR);
            ps.setInt(8, SSD);
            ps.setInt(9, PFR);
            ps.setInt(10, functionalTest);
            ps.setInt(11, systemTest);
            ps.setInt(12, regressionTest);
            ps.setInt(13, meeting);
            ps.setInt(14, lecture);
            ps.setInt(15, exercise);
            ps.setInt(16, computerExercise);
            ps.setInt(17, homeReading);
            ps.setInt(18, other);
            ps.setString(19, user);
            ps.setInt(20, week);


            System.out.println(ps);

            ps.executeUpdate();
            ps.close();
        } catch (SQLException ex) {
            printException(ex);
            resultOk = false;
        }
        return resultOk;
    }

    /**
     * Get all reports from a specific user.
     *
     * @param conn Name of the user.
     * @return String of all the reports from a user.
     */

    static String projectForm(Connection conn) {
        StringBuilder sb = new StringBuilder();

        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery("select * from Projects");
            sb.append("<p>Registrerade projekt:</p>");
            sb.append("<table border=\"1\"");
            sb.append("<tr><th>Projektnamn</td></tr>");
            while (rs.next()) {
                String name = rs.getString("projectName");
                sb.append("<tr>");
                sb.append("<td>" + name + "</td>");
                sb.append("</tr>");
            }
            sb.append("</table>");
            stmt.close();
        } catch (SQLException ex) {
            printException(ex);
        }
        return sb.toString();
    }

    static String getAllUserReports(String name, Connection conn) {
        PreparedStatement ps = null;
        StringBuilder sb = new StringBuilder();

        try {

            String sql = "SELECT week, status, comments FROM weeklyReports WHERE username = ?";
            ps = conn.prepareStatement(sql);
            ps.setString(1, name);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                sb.append("<tr>");
                sb.append("<td>" + rs.getInt("week") + "</td>");
                sb.append("<td>" + Database.statusName(rs.getInt("status")) + "</td>");
                sb.append("<td>" + "<a href = ReportPage?week=" + rs.getInt("week") + "&function=view> Visa </a>");
                if (rs.getInt("status") != 2) {
                    sb.append("<td>" + "<a href = ReportPage?week=" + rs.getInt("week") + "&function=update> Uppdatera </a>" + "</td>");
                } else {
                    sb.append("<td> </td>");
                }
                if (rs.getString("comments") == null) {
                    sb.append(" ");
                } else {
                    sb.append("<td>" + rs.getString("comments") + "</td>");
                }
                sb.append("</tr>");
            }

            ps.close();

        } catch (SQLException ex) {
            printException(ex);
        }
        return sb.toString();
    }

    /**
     * Get all reports from a specific user.
     *
     * @param name Name of the user.
     * @return String of all the reports from a user.
     */

    static String getAllUserReportsComplete(String name, Connection conn) {
        PreparedStatement ps = null;
        StringBuilder sb = new StringBuilder();

        try {

            String sql = "SELECT * FROM weeklyReports WHERE username = ?";
            ps = conn.prepareStatement(sql);
            ps.setString(1, name);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                sb.append("<tr>");
                sb.append("<td>" + rs.getString("username") + "</td>");
                sb.append("<td>" + rs.getInt("week") + "</td>");
                sb.append("<td>" + Database.statusName(rs.getInt("status")) + "</td>");
                sb.append("<td>" + "<a href = /ReportPage?week=" + rs.getInt("week") + "&function=view> Visa </a>");
                sb.append("<td><form>" + "<input type= \"radio\" name= \"function\" value=\"Accept\">");
                sb.append("<td>" + "<input type= \"radio\" name= \"function\" value=\"Deny\">");
                sb.append("<td>" + "<input type= \"submit\" value=\"Signera\"></td>");
                sb.append("<td>" + "<input type = \"text\" name =\"comment\" value = \"" + getStringNotNull(rs.getString("comments")) + "\">" +
                        "<input type = \"hidden\" name = \"user\" value = " + rs.getString("username") + ">" +
                        "<input type = \"hidden\" name = \"week\" value = " + rs.getInt("week") + ">" +
                        "</form></td>");
                sb.append("</tr>");

            }

            ps.close();

        } catch (SQLException ex) {
            printException(ex);
        }
        return sb.toString();
    }

    /**
     * Get all reports from a project.
     *
     * @param project Name of the project.
     * @return String of all the reports from a project.
     */


    public static String getAllProjectReports(String project, Connection conn) {
        PreparedStatement ps = null;
        StringBuilder sb = new StringBuilder();

        try {

            String sql = "SELECT * FROM weeklyReports LEFT JOIN Users ON weeklyReports.username = Users.username " +
                    "WHERE Users.project = ? AND activated <> '0' ORDER BY status, week, Users.username";
            ps = conn.prepareStatement(sql);
            ps.setString(1, project);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                sb.append("<tr>");
                sb.append("<td>" + rs.getString("username") + "</td>");
                sb.append("<td>" + rs.getInt("week") + "</td>");
                sb.append("<td>" + Database.statusName(rs.getInt("status")) + "</td>");
                sb.append("<td>" + "<a href = /ReportPage?week=" + rs.getInt("week") + "&function=view> Visa </a>");
                sb.append("<td><form>" + "<input type= \"radio\" name= \"function\" value=\"Accept\">");
                sb.append("<td>" + "<input type= \"radio\" name= \"function\" value=\"Deny\">");
                sb.append("<td>" + "<input type= \"submit\" value=\"Signera\"></td>");
                sb.append("<td>" + "<input type = \"text\" name =\"comment\" value = \"" + getStringNotNull(rs.getString("comments")) + "\">" +
                        "<input type = \"hidden\" name = \"user\" value = " + rs.getString("username") + ">" +
                        "<input type = \"hidden\" name = \"week\" value = " + rs.getInt("week") + ">" +
                        "</form></td>");
                sb.append("</tr>");
            }

            ps.close();

        } catch (SQLException ex) {
            System.out.println("SQLException: " + ex.getMessage());
//            System.out.println("SQLState: " + ex.getSQLState());
//            System.out.println("VendorError: " + ex.getErrorCode());
        }
        return sb.toString();
    }

    /**
     * Set null to empty Strings
     *
     * @param s String
     * @return String, if null then return "".
     */

    private static String getStringNotNull(String s) {
        if (s != null) {
            return s;
        }
        return "";
    }

    /**
     * Get a report.
     *
     * @param name Name of the user.
     * @param week Week of the report.
     * @return int array with the values of the report.
     */

    static int[] getReport(String name, int week, Connection conn) {
        PreparedStatement ps;
        int[] results = new int[18];

        try {

            String sql = "SELECT * FROM weeklyReports WHERE username=? AND week=?";
            ps = conn.prepareStatement(sql);
            ps.setString(1, name);
            ps.setInt(2, week);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                results[0] = rs.getInt("SDP");
                results[1] = rs.getInt("SRS");
                results[2] = rs.getInt("SVVS");
                results[3] = rs.getInt("STLDD");
                results[4] = rs.getInt("SVVI");
                results[5] = rs.getInt("SDDD");
                results[6] = rs.getInt("SVVR");
                results[7] = rs.getInt("SSD");
                results[8] = rs.getInt("PFR");
                results[9] = rs.getInt("functionalTest");
                results[10] = rs.getInt("systemTest");
                results[11] = rs.getInt("regressionTest");
                results[12] = rs.getInt("meeting");
                results[13] = rs.getInt("lecture");
                results[14] = rs.getInt("exercise");
                results[15] = rs.getInt("computerExercise");
                results[16] = rs.getInt("homeReading");
                results[17] = rs.getInt("other");
            }
            rs.close();
            ps.close();

        } catch (SQLException ex) {
            printException(ex);
        }
        return results;
    }


    /**
     * Get all report from a prpject.
     *
     * @param project Name of the project.
     * @return int array with the values of the report that belongs to a specific project.
     */

    public static int[] getAllReportsProject(String project, Connection conn) {
        PreparedStatement ps;
        int[] results = new int[18];

        try {

            String sql = "SELECT * FROM weeklyReports LEFT JOIN Users ON weeklyReports.username = Users.username WHERE Users.project = ?";
            ps = conn.prepareStatement(sql);
            ps.setString(1, project);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                results[0] += rs.getInt("SDP");
                results[1] += rs.getInt("SRS");
                results[2] += rs.getInt("SVVS");
                results[3] += rs.getInt("STLDD");
                results[4] += rs.getInt("SVVI");
                results[5] += rs.getInt("SDDD");
                results[6] += rs.getInt("SVVR");
                results[7] += rs.getInt("SSD");
                results[8] += rs.getInt("PFR");
                results[9] += rs.getInt("functionalTest");
                results[10] += rs.getInt("systemTest");
                results[11] += rs.getInt("regressionTest");
                results[12] += rs.getInt("meeting");
                results[13] += rs.getInt("lecture");
                results[14] += rs.getInt("exercise");
                results[15] += rs.getInt("computerExercise");
                results[16] += rs.getInt("homeReading");
                results[17] += rs.getInt("other");
            }
            rs.close();
            ps.close();

        } catch (SQLException ex) {
            printException(ex);
        }
        return results;
    }


    /**
     * Deny a report
     *
     * @param name Name of the user.
     * @param week Week of the report.
     */

    static void denyReport(String name, int week, Connection conn) {

        PreparedStatement ps = null;
        try {
            String sql = "UPDATE weeklyReports SET status = '" + 1 + "' WHERE username= ? AND week = ?";
            ps = conn.prepareStatement(sql);
            ps.setString(1, name);
            ps.setInt(2, week);

            //ResultSet rs = ps.executeQuery(sql);

            ps.executeUpdate();
            ps.close();

        } catch (SQLException ex) {
            printException(ex);
        }
    }

    /**
     * Accept a report
     *
     * @param name Name of the user.
     * @param week Week of the report.
     * @return true if it got accepted, else false.
     */

    public static boolean acceptReport(String name, int week, Connection conn) {

        boolean resultOk = true;
        PreparedStatement ps = null;
        try {
            String sql = "UPDATE weeklyReports SET status = '" + 2 + "' WHERE username= ? AND week = ?";
            ps = conn.prepareStatement(sql);
            ps.setString(1, name);
            ps.setInt(2, week);

            //ResultSet rs = ps.executeQuery(sql);

            ps.executeUpdate();
            ps.close();

        } catch (SQLException ex) {
            resultOk = false;
            printException(ex);
        }
        return resultOk;
    }

    /**
     * Get the role of a user.
     *
     * @param username Name of the user.
     * @return int of the users role.
     */

    public static int getRole(String username, Connection conn) {

        PreparedStatement ps = null;
        try {
            String sql = "SELECT role FROM Users WHERE username = ?";
            ps = conn.prepareStatement(sql);
            ps.setString(1, username);

            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                return rs.getInt("role");
            }

            ps.close();

        } catch (SQLException ex) {
            printException(ex);
        }
        return 404;
    }

    /**
     * Get the project for a user.
     *
     * @param username Name of the user.
     * @return String with the name of the project where the user belongs.
     */

    public static String getProject(String username, Connection conn) {
        PreparedStatement ps;
        try {
            String sql = "SELECT project FROM Users WHERE username = ?";
            ps = conn.prepareStatement(sql);
            ps.setString(1, username);

            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                return rs.getString("project");
            }

            ps.close();

        } catch (SQLException ex) {
            printException(ex);
        }
        return null;
    }

    /**
     * Get user from a project.
     *
     * @param project Name of the project.
     * @return String with users name in the project.
     */

    public static String getUser(String project, Connection conn) {
        PreparedStatement ps;
        try {
            String sql = "SELECT username FROM Users WHERE project = ?";
            ps = conn.prepareStatement(sql);
            ps.setString(1, project);

            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                return rs.getString("username");
            }

            ps.close();

        } catch (SQLException ex) {
            printException(ex);
        }
        return null;
    }

    /**
     * Print exceptions
     *
     * @param ex type of SQLExcepton.
     */

    private static void printException(SQLException ex) {
        System.out.println("SQLException: " + ex.getMessage());
        System.out.println("SQLState: " + ex.getSQLState());
        System.out.println("VendorError: " + ex.getErrorCode());
    }


    /**
     * Get status for a report
     *
     * @param n status for report.
     * @return String with text for status.
     */

    private static String statusName(int n) {
        String s = "Inskickad";

        if (n == 1) {
            s = "Nekad";
        } else if (n == 2) {
            s = "Accepterad";
        }
        return s;
    }

    /**
     * Get weeks of the project
     *
     * @param project Name of the project.
     * @return ArrayList with the weeks of the project.
     */

    public static ArrayList<Integer> getWeeksOfProject(String project, Connection conn) {
        PreparedStatement ps;
        ArrayList<Integer> results = new ArrayList<Integer>();

        try {

            String sql = "SELECT week FROM weeklyReports LEFT JOIN Users ON weeklyReports.username = Users.username WHERE Users.project = ? " +
                    "GROUP BY week";
            ps = conn.prepareStatement(sql);
            ps.setString(1, project);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                results.add(rs.getInt("week"));
            }
            rs.close();
            ps.close();

        } catch (SQLException ex) {
            System.out.println("SQLException: " + ex.getMessage());
        }
        return results;
    }

    /**
     * Get a specific report
     *
     * @param project Name of the project.
     * @param week    Week of the report.
     * @return Values of the columns.
     */

    public static int getSpecificWeekReports(String project, String week, Connection conn) {
        PreparedStatement ps;
        int results = 0;

        try {

            String sql = "SELECT * FROM weeklyReports LEFT JOIN Users ON weeklyReports.username = Users.username WHERE Users.project = ? AND week=? AND activated = 1";
            ps = conn.prepareStatement(sql);
            ps.setString(1, project);
            ps.setString(2, week);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                results += rs.getInt("SDP");
                results += rs.getInt("SRS");
                results += rs.getInt("SVVS");
                results += rs.getInt("STLDD");
                results += rs.getInt("SVVI");
                results += rs.getInt("SDDD");
                results += rs.getInt("SVVR");
                results += rs.getInt("SSD");
                results += rs.getInt("PFR");
                results += rs.getInt("functionalTest");
                results += rs.getInt("systemTest");
                results += rs.getInt("regressionTest");
                results += rs.getInt("meeting");
                results += rs.getInt("lecture");
                results += rs.getInt("exercise");
                results += rs.getInt("computerExercise");
                results += rs.getInt("homeReading");
                results += rs.getInt("other");
            }
            rs.close();
            ps.close();

        } catch (SQLException ex) {
            System.out.println("SQLException: " + ex.getMessage());
//            System.out.println("SQLState: " + ex.getSQLState());
//            System.out.println("VendorError: " + ex.getErrorCode());
        }
        return results;
    }

    /**
     * Get all week reports connected to a project
     *
     * @param project name of the project
     * @return int combined time for all the weekly reports of the project
     */

    public static int getAllWeekReports(String project, Connection conn) {
        PreparedStatement ps;
        int results = 0;

        try {

            String sql = "SELECT * FROM weeklyReports LEFT JOIN Users ON weeklyReports.username = Users.username WHERE Users.project = ? and Users.activated = 1";
            ps = conn.prepareStatement(sql);
            ps.setString(1, project);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                results += rs.getInt("SDP");
                results += rs.getInt("SRS");
                results += rs.getInt("SVVS");
                results += rs.getInt("STLDD");
                results += rs.getInt("SVVI");
                results += rs.getInt("SDDD");
                results += rs.getInt("SVVR");
                results += rs.getInt("SSD");
                results += rs.getInt("PFR");
                results += rs.getInt("functionalTest");
                results += rs.getInt("systemTest");
                results += rs.getInt("regressionTest");
                results += rs.getInt("meeting");
                results += rs.getInt("lecture");
                results += rs.getInt("exercise");
                results += rs.getInt("computerExercise");
                results += rs.getInt("homeReading");
                results += rs.getInt("other");
            }
            rs.close();
            ps.close();

        } catch (SQLException ex) {
            System.out.println("SQLException: " + ex.getMessage());
//            System.out.println("SQLState: " + ex.getSQLState());
//            System.out.println("VendorError: " + ex.getErrorCode());
        }
        return results;

    }

    /**
     * Get the users of a project
     *
     * @param project name of the project
     * @return ArrayList<String> with the users
     */

    public static ArrayList<String> getUsers(String project, Connection conn) {
        PreparedStatement ps;
        ArrayList<String> results = new ArrayList<String>();
        try {
            String sql = "SELECT username FROM Users WHERE project = ?";
            ps = conn.prepareStatement(sql);
            ps.setString(1, project);

            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                results.add(rs.getString("username"));
            }

            ps.close();

        } catch (SQLException ex) {
            printException(ex);
        }
        return results;
    }


    /**
     * Print weekly reports for a specific user
     *
     * @param project name of the project
     * @param user    name of the user
     * @return ArrayList<String> with the weekly reports for the user
     */

    public static ArrayList<String> printSpecificHistory(String project, String user, Connection conn) {
        PreparedStatement ps;
        ArrayList<String> results = new ArrayList<String>();
        try {
            String sql = "SELECT username, weeks, comments FROM WeeklyReports WHERE project = ? AND username = ?";
            ps = conn.prepareStatement(sql);
            ps.setString(1, project);
            ps.setString(2, user);

            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                results.add(rs.getString("username"));
            }

            ps.close();

        } catch (SQLException ex) {
            printException(ex);
        }
        return results;
    }

    public String getComement(String user, int week, Connection conn) {
        PreparedStatement ps;
        String hej = "Inga kommentarer";
        try {
            String sql = "SELECT comments FROM weeklyReports WHERE week =?  AND username = ?";
            ps = conn.prepareStatement(sql);
            ps.setInt(1, week);
            ps.setString(2, user);

            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                hej = rs.getString("comments");
            }
            ps.close();

        } catch (SQLException ex) {
            printException(ex);
        }
        return hej;
    }

}