import java.sql.Connection;

import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * This class is the superclass for all servlets in the application.
 * It includes basic functionality required by many servlets, like for example a page head
 * written by all servlets, and the connection to the database.
 * <p>
 * This application requires a database.
 * For username and password, see the constructor in this class.
 *
 * <p>The database can be created with the following SQL command:
 * mysql> create database base;
 * <p>The required table can be created with created with:
 * mysql> create table users(name varchar(10), password varchar(10), primary key (name));
 * <p>The administrator can be added with:
 * mysql> insert into users (name, password) values('admin', 'adminp');
 *
 * @author Martin Host
 * @version 1.0
 */
public class servletBase extends HttpServlet {

    private static final long serialVersionUID = 1L;

    // Define states
    protected static final int LOGIN_FALSE = 0;
    protected static final int LOGIN_TRUE = 1;
    protected Connection conn = null;
    private boolean isLoggedIn = false;

    /**
     * Constructs a servlet and makes a connection to the database.
     * It also writes all user names on the console for test purpose.
     */
    public servletBase() {
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            conn = DriverManager.getConnection("jdbc:mysql://vm23.cs.lth.se/pusp1901hbg", "pusp1901hbg", "je3jd89g");


            // Display the contents of the database in the console.
            // This should be removed in the final version
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery("select * from Users");
            while (rs.next()) {
                String name = rs.getString("username");
                System.out.println("base " + name);
            }

            stmt.close();

        } catch (SQLException ex) {
            System.out.println("SQLException: " + ex.getMessage());
            System.out.println("SQLState: " + ex.getSQLState());
            System.out.println("VendorError: " + ex.getErrorCode());
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    /**
     * Checks if a user is logged in or not.
     * @param request The HTTP Servlet request (so that the session can be found)
     * @return true if the user is logged in, otherwise false.
     */
    protected static boolean loggedIn(HttpServletRequest request) {
        HttpSession session = request.getSession(true);
        Object objectState = session.getAttribute("state");
        int state = LOGIN_FALSE;
        if (objectState != null)
            state = (Integer) objectState;
        return (state == LOGIN_TRUE);
    }

    /**
     * Can be used to construct form elements.
     * @param par Input string
     * @return output string = "par"
     */
    protected String formElement(String par) {
    	return '"' + par + '"';
    }


    /**
     * Constructs the header of all servlets.
     *
     * @return String with html code for the header.
     */
    protected String getPageIntro() {
        String intro = "<html>" +
                "<head><title> Tidrapporterings System </title>" +
                "<script type=\"text/javascript\"> \n" +
                "var end = 0;\n" +
                "var endTime = 1200;" +
                "var Interval = setInterval(function(){ isIdle() }, 1000); \n" +
                "function isIdle() { \n" +
                "end = end + 1; \n" +
                "page = window.location.pathname;" +
                "if (end > endTime && page != \"/\") { \n" +
                "var url= \"/\"; \n" +
                "window.location = url; \n" +
                "clearInterval (Interval);" +
                "alert (\" Du har varit inaktiv i 20 minuter. Var god logga in igen.\")" +
                "} \n" +

                "} \n" +
                "</script>" +

                "</head>" +
                "<body onload=\"isIdle()\">";
        return intro;
    }

    protected String getFunctionMenu() {
        String html = "<head>\n" +
                "    <meta charset=\"ISO-8859-1\">\n" +
                "    <title>Title</title>\n" +
                "    <style> a {\n" +
                "    text-decoration: none;\n" +
                "        line-height: 16px;\n" +
                "    }\n" +
                "\n" +
                "\n" +
                "    </style>\n" +
                "</head>\n" +
                "<body BGCOLOR=\"#add8e6\">\n" +
                "<!-- menu row --><TR VALIGN=top>\n" +
                "    <TD NOWRAP>\n" +
                "\n" +
                "\n" +
                "        <BR>\n" +
                "        <A  HREF=\"MainPage\" ><font color=\"#f8f8ff\" > Startsida </font></A>\n" +
                "        <BR>\n" +
                "        <A HREF=\"ReportPage\"><font color=\"black\"> Ny rapport </font></A>\n" +
                "        <BR>\n" +
                "        <A HREF=\"ReportHistoryPage\"><font color=\"#f8f8ff\"> Se rapporter </font></A>\n" +
                "        <BR>\n" +
                "        <A HREF=\"SummaryPage\"><font color=\"black\">Se sammanställning </font></A>\n" +
                "        <BR>\n" +
                "        <A HREF=\"Administration\">   <font color=\"#f8f8ff\">Administration</font></A>\n" +
                "        <BR>\n" +
                "        <A HREF=\"ProjectPage\">   <font color=\"black\">Projekt</font></A>\n" +
                "        <BR>\n" +
                "        <A HREF=\"SignPage\">   <font color=\"#f8f8ff\">Signering</font></A>\n" +
                "        <BR>\n" +
                "        <A HREF=\"/pusp1901\">   <font color=\"black\">Logga ut</font></A>\n" +
                "\n" +
                "    </TD>\n" +
                "</TR>";
        return html;
    }

}
