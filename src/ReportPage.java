import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

/**
 * Servlet implementation class ReportPage.
 *
 * @author Martin Host
 * @version 1.0
 */
@WebServlet("/ReportPage")
public class ReportPage extends servletBase {

    /**
     * @see servletBase#servletBase()
     */
    public ReportPage() {
        super();
    }

    /**
     * Handles input from the user and displays information.
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        HttpSession session = request.getSession(true);
        PrintWriter out = response.getWriter();
        if(loggedIn(request)) {
            out.println(getPageIntro());
            out.println(getFunctionMenu());


            if (request.getParameter("function") != null) {
                if (request.getParameter("function").equals("newReport")) {
                    Database.createNewReport(Integer.parseInt(request.getParameter("week")), (String) session.getAttribute("name"), Integer.parseInt(request.getParameter("SDP")), Integer.parseInt(request.getParameter("SRS")), Integer.parseInt(request.getParameter("SVVS")), Integer.parseInt(request.getParameter("STLDD")), Integer.parseInt(request.getParameter("SVVI")),
                            Integer.parseInt(request.getParameter("SDDD")), Integer.parseInt(request.getParameter("SVVR")), Integer.parseInt(request.getParameter("SSD")), Integer.parseInt(request.getParameter("PFR")), Integer.parseInt(request.getParameter("functionalTest")),
                            Integer.parseInt(request.getParameter("systemTest")), Integer.parseInt(request.getParameter("regressionTest")), Integer.parseInt(request.getParameter("meeting")), Integer.parseInt(request.getParameter("lecture")),
                            Integer.parseInt(request.getParameter("exercise")), Integer.parseInt(request.getParameter("computerExercise")), Integer.parseInt(request.getParameter("homeReading")), Integer.parseInt(request.getParameter("other")), conn);
                } else if (request.getParameter("function").equals("update")) {
                    out.println("<h1>Uppdatera tidrapport </h1>");
                    int[] reportValues = Database.getReport((String) session.getAttribute("name"), Integer.parseInt(request.getParameter("week")), conn);
                    out.println(updateReportRequestForm(reportValues, Integer.parseInt(request.getParameter("week"))));
                } else if (request.getParameter("function").equals("updateReport")) {
                    if (Database.updateReport(Integer.parseInt(request.getParameter("week")), (String) session.getAttribute("name"), Integer.parseInt(request.getParameter("SDP")), Integer.parseInt(request.getParameter("SRS")), Integer.parseInt(request.getParameter("SVVS")), Integer.parseInt(request.getParameter("STLDD")), Integer.parseInt(request.getParameter("SVVI")),
                            Integer.parseInt(request.getParameter("SDDD")), Integer.parseInt(request.getParameter("SVVR")), Integer.parseInt(request.getParameter("SSD")), Integer.parseInt(request.getParameter("PFR")), Integer.parseInt(request.getParameter("functionalTest")),
                            Integer.parseInt(request.getParameter("systemTest")), Integer.parseInt(request.getParameter("regressionTest")), Integer.parseInt(request.getParameter("meeting")), Integer.parseInt(request.getParameter("lecture")),
                            Integer.parseInt(request.getParameter("exercise")), Integer.parseInt(request.getParameter("computerExercise")), Integer.parseInt(request.getParameter("homeReading")), Integer.parseInt(request.getParameter("other")), conn)) {
                        response.sendRedirect("MainPage");
                    } else {
                        response.sendRedirect("");
                    }

                } else if (request.getParameter("function").equals("view")) {
                    out.println("<h1>Visa tidrapport </h1>");
                    int[] viewValues = Database.getReport((String) session.getAttribute("name"), Integer.parseInt(request.getParameter("week")), conn);
                    out.println(ViewReportRequest(viewValues, Integer.parseInt(request.getParameter("week"))));

                }
            } else {
                out.println("<h1>Ny tidrapport </h1>");
                out.println(newReportRequestForm(request));
            }

            out.println("</body></html>");
        }else{
            response.sendRedirect("");
        }
    }

    private String ViewReportRequest(int[] viewValues, int week) {
        String html = "<body BGCOLOR=\"#add8e6\">";
        html += "<form name =\"rapport\" > <table>";
        html += "<tr>" +
                "    <th>Vecka:</th>" +
                "    <th><input type=" + formElement("numb") + "name=\"week\" value=" + week + " ></td>\n</th>" +
                "  </tr>";
        html += "<tr>" +
                "    <th>Nummer</th>" +
                "    <th>Aktivitet</th> " +
                "    <th>Total tid</th>" +
                "  </tr>";
        html += "<tr>" +
                "    <td>11</td><BR>" +
                "    <td>SDP</td> " +
                "    <td><input type=" + formElement("numb") + "name=\"SDP\" value=" + viewValues[0] + "></td>" +
                "  </tr>";
        html += "<tr>" +
                "    <td>12</td>" +
                "    <td>SRS</td> " +
                "    <td><input type=" + formElement("numb") + "name=\"SRS\" value=" + viewValues[1] + "></td>" +
                "  </tr>";
        html += "<tr>" +
                "    <td>13</td>" +
                "    <td>SVVS</td> " +
                "    <td><input type=" + formElement("numb") + "name=\"SVVS\" value=" + viewValues[2] + "></td>" +
                "  </tr>";
        html += "<tr>" +
                "    <td>14</td>" +
                "    <td>STLDD</td> " +
                "    <td><input type=" + formElement("numb") + "name=\"STLDD\" value=" + viewValues[3] + "></td>" +
                "  </tr>";
        html += "<tr>" +
                "    <td>15</td>" +
                "    <td>SVVI</td> " +
                "    <td><input type=" + formElement("numb") + "name=\"SVVI\" value=" + viewValues[4] + "></td>" +
                "  </tr>";
        html += "<tr>" +
                "    <td>16</td>" +
                "    <td>SDDD</td> " +
                "    <td><input type=" + formElement("numb") + "name=\"SDDD\" value=" + viewValues[5] + "></td>" +
                "  </tr>";
        html += "<tr>" +
                "    <td>17</td>" +
                "    <td>SVVR</td> " +
                "    <td><input type=" + formElement("numb") + "name=\"SVVR\" value=" + viewValues[6] + "></td>" +
                "  </tr>";
        html += "<tr>" +
                "    <td>18</td>" +
                "    <td>SSD</td> " +
                "    <td><input type=" + formElement("numb") + "name=\"SSD\" value=" + viewValues[7] + "></td>" +
                "  </tr>";
        html += "<tr>" +
                "    <td>19</td>" +
                "    <td>Slutrapport</td> " +
                "    <td><input type=" + formElement("numb") + "name=\"PFR\" value=" + viewValues[8] + "></td>" +
                "  </tr>";
        html += "<tr>" +
                "    <td>100</td>" +
                "    <td>Hemarbete</td>" +
                "    <td><input type=" + formElement("numb") + "name=\"homeReading\" value=" + viewValues[9] + "></td>" +
                "  </tr>";
        html += "<tr>" +
                "    <td>21</td>" +
                "    <td>Funktionstest</td> " +
                "    <td><input type=" + formElement("numb") + "name=\"functionalTest\" value=" + viewValues[10] + "></td>" +
                "  </tr>";
        html += "<tr>" +
                "    <td>22</td>" +
                "    <td>Systemtest</td> " +
                "    <td><input type=" + formElement("numb") + "name=\"systemTest\" value=" + viewValues[11] + "></td>" +
                "  </tr>";
        html += "<tr>" +
                "    <td>23</td>" +
                "    <td>Regressionstest</td> " +
                "    <td><input type=" + formElement("numb") + "name=\"regressionTest\" value=" + viewValues[12] + "></td>" +
                "  </tr>";
        html += "<tr>" +
                "    <td>30</td>" +
                "    <td>Möte</td> " +
                "    <td><input type=" + formElement("numb") + "name=\"meeting\" value=" + viewValues[13] + "></td>" +
                "  </tr>";
        html += "<tr>" +
                "    <td>41</td>" +
                "    <td>Föreläsning</td>" +
                "    <td><input type=" + formElement("numb") + "name=\"lecture\" value=" + viewValues[14] + "></td>" +
                "  </tr>";
        html += "<tr>" +
                "    <td>42</td>" +
                "    <td>Övning</td> " +
                "    <td><input type=" + formElement("numb") + "name=\"exercise\" value=" + viewValues[15] + "></td>" +
                "  </tr>";
        html += "<tr>" +
                "    <td>43</td>" +
                "    <td>Laboration</td> " +
                "    <td><input type=" + formElement("numb") + "name=\"computerExercise\" value=" + viewValues[16] + "></td>" +
                "  </tr>";
        html += "<tr>" +
                "    <td>44</td>" +
                "    <td>Övrigt</td> " +
                "    <td><input type=" + formElement("numb") + "name=\"other\" value=" + viewValues[17] + "></td>" +
                "  </tr>" +
                "</table>";
        return html;
    }

    private String updateReportRequestForm(int[] reportValues, int week) {
        String html = "<body BGCOLOR=\"#add8e6\">" + "<p>Fyll i tidrapporten. Det enda obligatoriska fältet är vecka.</p>" +
                "<p>För att skicka in, klicka på knappen \"Skicka\".</p>";

        html += "<form> <table>";
        html += "<tr>" +
                "    <th>Vecka:</th>" +
                "    <th><input type=" + formElement("number") + "name=\"week\" value=" + week + " ></td>\n</th>" +
                "  </tr>";
        html += "<tr>" +
                "    <th>Nummer</th>" +
                "    <th>Aktivitet</th> " +
                "    <th>Total tid</th>" +
                "  </tr>";
        html += "<tr>" +
                "    <td>11</td><BR>" +
                "    <td>SDP</td> " +
                "    <td><input id=1 type=" + formElement("number") + "name=\"SDP\" value=" + reportValues[0] + "></td>" +
                "  </tr>";
        html += "<tr>" +
                "    <td>12</td>" +
                "    <td>SRS</td> " +
                "    <td><input id=2 type=" + formElement("number") + "name=\"SRS\" value=" + reportValues[1] + "></td>" +
                "  </tr>";
        html += "<tr>" +
                "    <td>13</td>" +
                "    <td>SVVS</td> " +
                "    <td><input id =3 type=" + formElement("number") + "name=\"SVVS\" value=" + reportValues[2] + "></td>" +
                "  </tr>";
        html += "<tr>" +
                "    <td>14</td>" +
                "    <td>STLDD</td> " +
                "    <td><input id=4 type=" + formElement("number") + "name=\"STLDD\" value=" + reportValues[3] + "></td>" +
                "  </tr>";
        html += "<tr>" +
                "    <td>15</td>" +
                "    <td>SVVI</td> " +
                "    <td><input id = 5 type=" + formElement("number") + "name=\"SVVI\" value=" + reportValues[4] + "></td>" +
                "  </tr>";
        html += "<tr>" +
                "    <td>16</td>" +
                "    <td>SDDD</td> " +
                "    <td><input id = 6 type=" + formElement("number") + "name=\"SDDD\" value=" + reportValues[5] + "></td>" +
                "  </tr>";
        html += "<tr>" +
                "    <td>17</td>" +
                "    <td>SVVR</td> " +
                "    <td><input type=" + formElement("number") + "name=\"SVVR\" value=" + reportValues[6] + "></td>" +
                "  </tr>";
        html += "<tr>" +
                "    <td>18</td>" +
                "    <td>SSD</td> " +
                "    <td><input type=" + formElement("number") + "name=\"SSD\" value=" + reportValues[7] + "></td>" +
                "  </tr>";
        html += "<tr>" +
                "    <td>19</td>" +
                "    <td>Slutrapport</td> " +
                "    <td><input type=" + formElement("number") + "name=\"PFR\" value=" + reportValues[8] + "></td>" +
                "  </tr>";
        html += "<tr>" +
                "    <td>100</td>" +
                "    <td>Hemarbete</td>" +
                "    <td><input type=" + formElement("number") + "name=\"homeReading\" value=" + reportValues[9] + "></td>" +
                "  </tr>";
        html += "<tr>" +
                "    <td>21</td>" +
                "    <td>Funktionstest</td> " +
                "    <td><input type=" + formElement("number") + "name=\"functionalTest\" value=" + reportValues[10] + "></td>" +
                "  </tr>";
        html += "<tr>" +
                "    <td>22</td>" +
                "    <td>Systemtest</td> " +
                "    <td><input type=" + formElement("number") + "name=\"systemTest\" value=" + reportValues[11] + "></td>" +
                "  </tr>";
        html += "<tr>" +
                "    <td>23</td>" +
                "    <td>Regressionstest</td> " +
                "    <td><input type=" + formElement("number") + "name=\"regressionTest\" value=" + reportValues[12] + "></td>" +
                "  </tr>";
        html += "<tr>" +
                "    <td>30</td>" +
                "    <td>Möte</td> " +
                "    <td><input type=" + formElement("number") + "name=\"meeting\" value=" + reportValues[13] + "></td>" +
                "  </tr>";
        html += "<tr>" +
                "    <td>41</td>" +
                "    <td>Föreläsning</td>" +
                "    <td><input type=" + formElement("number") + "name=\"lecture\" value=" + reportValues[14] + "></td>" +
                "  </tr>";
        html += "<tr>" +
                "    <td>42</td>" +
                "    <td>Övning</td> " +
                "    <td><input type=" + formElement("number") + "name=\"exercise\" value=" + reportValues[15] + "></td>" +
                "  </tr>";
        html += "<tr>" +
                "    <td>43</td>" +
                "    <td>Laboration</td> " +
                "    <td><input type=" + formElement("number") + "name=\"computerExercise\" value=" + reportValues[16] + "></td>" +
                "  </tr>";
        html += "<tr>" +
                "    <td>44</td>" +
                "    <td>Övrigt</td> " +
                "    <td><input type=" + formElement("number") + "name=\"other\" value=" + reportValues[17] + "></td>" +
                "  </tr>";
        html += "</table>" +
                "<input type=" + formElement("submit") + "value=\"Uppdatera\">" +
                "<input type=\"hidden\" name=\"function\" value=\"updateReport\">" +
                " </from>";
        return html;
    }

    private String newReportRequestForm(HttpServletRequest request) {
        HttpSession session = request.getSession(true);
        LocalDate date = LocalDate.now();
        Calendar calendar = new GregorianCalendar();
        Date trialTime = new Date();
        calendar.setTime(trialTime);
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
        String html = "<body BGCOLOR=\"#add8e6\">" + "<p>Fyll i tidrapporten. Det enda obligatoriska fältet är vecka.</p>" +
                "<p>För att skicka in, klicka på knappen \"Skicka\".</p>";

        html += "<form name = \"rep\"> <table>";
        html += "<tr>" +
                "    <th>Namn:</th>" +
                "    <td>" + session.getAttribute("name") + "</th>" +
                "    <th>Datum:</th>" +
                "    <th>" + date.format(formatter) + "</th>" +
                "  </tr>";
        html += "<tr>" +
                "    <th>Projektgrupp:</th>" +
                "    <td>" + session.getAttribute("project") + "</th>" +
                "    <th>Vecka: </th>" +
                "    <th><input type=" + formElement("number") + "name=\"week\" value=\"" + calendar.get(Calendar.WEEK_OF_YEAR) + "\"></td>\n</th>" +
                "  </tr>";
        html += "<tr>" +
                "    <th>Nummer</th>" +
                "    <th>Aktivitet</th> " +
                "    <th>Total tid</th>" +
                "  </tr>";
        html += "<tr>" +
                "    <td>11</td><BR>" +
                "    <td>SDP</td> " +
                "    <td><input id = 1 type= " + formElement("number") + "name=\"SDP\" value=0 onkeypress='return event.charCode >= 48 && event.charCode <= 57'></td>" +
                "  </tr>";
        html += "<tr>" +
                "    <td>12</td>" +
                "    <td>SRS</td> " +
                "    <td><input id = 2 type=" + formElement("number") + "name=\"SRS\" value=0 onkeypress='return event.charCode >= 48 && event.charCode <= 57'></td>" +
                "  </tr>";
        html += "<tr>" +
                "    <td>13</td>" +
                "    <td>SVVS</td> " +
                "    <td><input id = 3 type=" + formElement("number") + "name=\"SVVS\" value=0 onkeypress='return event.charCode >= 48 && event.charCode <= 57'></td>" +
                "  </tr>";
        html += "<tr>" +
                "    <td>14</td>" +
                "    <td>STLDD</td> " +
                "    <td><input id = 4 type=" + formElement("number") + "name=\"STLDD\" value=0 onkeypress='return event.charCode >= 48 && event.charCode <= 57'></td>" +
                "  </tr>";
        html += "<tr>" +
                "    <td>15</td>" +
                "    <td>SVVI</td> " +
                "    <td><input id = 5 type=" + formElement("number") + "name=\"SVVI\" value=0 onkeypress='return event.charCode >= 48 && event.charCode <= 57'></td>" +
                "  </tr>";
        html += "<tr>" +
                "    <td>16</td>" +
                "    <td>SDDD</td> " +
                "    <td><input id = 6 type=" + formElement("number") + "name=\"SDDD\" value=0 onkeypress='return event.charCode >= 48 && event.charCode <= 57'></td>" +
                "  </tr>";
        html += "<tr>" +
                "    <td>17</td>" +
                "    <td>SVVR</td> " +
                "    <td><input id = 7 type=" + formElement("number") + "name=\"SVVR\" value=0 onkeypress='return event.charCode >= 48 && event.charCode <= 57'></td>" +
                "  </tr>";
        html += "<tr>" +
                "    <td>18</td>" +
                "    <td>SSD</td> " +
                "    <td><input id = 8 type=" + formElement("number") + "name=\"SSD\" value=0 onkeypress='return event.charCode >= 48 && event.charCode <= 57'></td>" +
                "  </tr>";
        html += "<tr>" +
                "    <td>19</td>" +
                "    <td>Slutrapport</td> " +
                "    <td><input id = 9 type=" + formElement("number") + "name=\"PFR\" value=0 onkeypress='return event.charCode >= 48 && event.charCode <= 57'></td>" +
                "  </tr>";
        html += "<tr>" +
                "    <td>100</td>" +
                "    <td>Hemarbete</td>" +
                "    <td><input id = 10 type=" + formElement("number") + "name=\"homeReading\" value=0 onkeypress='return event.charCode >= 48 && event.charCode <= 57'></td>" +
                "  </tr>";
        html += "<tr>" +
                "    <td>21</td>" +
                "    <td>Funktionstest</td> " +
                "    <td><input id = 12 type=" + formElement("number") + "name=\"functionalTest\" value=0 onkeypress='return event.charCode >= 48 && event.charCode <= 57'></td>" +
                "  </tr>";
        html += "<tr>" +
                "    <td>22</td>" +
                "    <td>Systemtest</td> " +
                "    <td><input id = 13 type=" + formElement("number") + "name=\"systemTest\" value=0 onkeypress='return event.charCode >= 48 && event.charCode <= 57'></td>" +
                "  </tr>";
        html += "<tr>" +
                "    <td>23</td>" +
                "    <td>Regressionstest</td> " +
                "    <td><input id = 14 type=" + formElement("number") + "name=\"regressionTest\" value=0 onkeypress='return event.charCode >= 48 && event.charCode <= 57'></td>" +
                "  </tr>";
        html += "<tr>" +
                "    <td>30</td>" +
                "    <td>Möte</td> " +
                "    <td><input id = 15 type=" + formElement("number") + "name=\"meeting\" value=0 onkeypress='return event.charCode >= 48 && event.charCode <= 57'></td>" +
                "  </tr>";
        html += "<tr>" +
                "    <td>41</td>" +
                "    <td>Föreläsning</td>" +
                "    <td><input id = 16 type=" + formElement("number") + "name=\"lecture\" value=0 onkeypress='return event.charCode >= 48 && event.charCode <= 57'></td>" +
                "  </tr>";
        html += "<tr>" +
                "    <td>42</td>" +
                "    <td>Övning</td> " +
                "    <td><input id = 17 type=" + formElement("number") + "name=\"exercise\" value=0 onkeypress='return event.charCode >= 48 && event.charCode <= 57'></td>" +
                "  </tr>";
        html += "<tr>" +
                "    <td>43</td>" +
                "    <td>Laboration</td> " +
                "    <td><input id = 18 type=" + formElement("number") + "name=\"computerExercise\" value=0 onkeypress='return event.charCode >= 48 && event.charCode <= 57'></td>" +
                "  </tr>";
        html += "<tr>" +
                "    <td>44</td>" +
                "    <td>Övrigt</td> " +
                "    <td><input id = 19 type=" + formElement("number") + "name=\"other\" value=0 onkeypress='return event.charCode >= 48 && event.charCode <= 57'></td>" +
                "  </tr>";
        html += "</table>" +
                "<input type=" + formElement("submit") + "value=\"Skicka\">" +
                "<input type=\"hidden\" name=\"function\" value=\"newReport\">";
        html += "<script> function update(){  sum = document.forms[\"rep\"][\"SDP\"].value +document.forms[\"rep\"][\"SRS\"].value +document.forms[\"rep\"][\"SVVS\"].value +document.forms[\"rep\"][\"STLDD\"].value +document.forms[\"rep\"][\"SVVI\"].value +document.forms[\"rep\"][\"SDDD\"].value +document.forms[\"rep\"][\"SVVR\"].value +document.forms[\"rep\"][\"SSD\"].value +document.forms[\"rep\"][\"Final Report\"].value +document.forms[\"rep\"][\"Home Reading\"].value +document.forms[\"rep\"][\"Functional test\"].value +document.forms[\"rep\"][\"System test\"].value +document.forms[\"rep\"][\"Regression test\"].value +document.forms[\"rep\"][\"Meeting\"].value +document.forms[\"rep\"][\"Lecture\"].value +document.forms[\"rep\"][\"Exercise\"].value +document.forms[\"rep\"][\"Computer Exercise\"].value +document.forms[\"rep\"][\"Other\"].value;  document.calc.summa.value = sum;}update();\n" +
                "</script>" + " </form>";

        return html;
    }
}