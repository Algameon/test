import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Servlet implementation class MainPage.
 *  @author Martin Host
 *  @version 1.0
 */
@WebServlet("/MainPage")
public class MainPage extends servletBase {

    /**
     * @see servletBase#servletBase()
     */
    public MainPage() {
        super();
    }

    /**
     * Handles input from the user and displays information.
     *
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        PrintWriter out = response.getWriter();
        if(loggedIn(request)){

            out.println(getPageIntro());
            out.println(getFunctionMenu());
        }else{
            response.sendRedirect("");
        }

    }

    /**
     *
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response) {
        // TODO Auto-generated method stub
    }
}